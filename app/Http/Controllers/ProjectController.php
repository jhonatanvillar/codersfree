<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllProjects()
    {
        // $projects = Project::all();
        // $projects = Project::chunk(200, function ($projects) {
        //     foreach ($projects as $project) {
        //         //Aquí escribimos lo que haremos con los datos (operar, modificar, etc)
        //     }
        // });
        $projects = Project::where('is_active', '=', 2)->firstOrFail();
        return $projects;
        // return view('eloquentViews/project',compact('projects'));
    }
    public function random(Request $request){
        return "hola";
    }
    public function insertProject(Request $request)
    {

        $project = new Project;
        $project->city_id = 1;
        $project->company_id = 1;
        $project->user_id = 1;
        $project->name = 'Nombre del proyecto';
        $project->execution_date = '2020-04-30';
        $project->is_active = 1;
        $project->save();

        return "Guardado";
    }

    public function index()
    {
        // $datos = DB::table('projects')->get();
        // return Project::create([
        //     'city_id' => 2,
        //     'company_id' => 2,
        //     'user_id' => 2,
        //     'name' => 'probando Nombre del proyecto',
        //     'execution_date' => now(),
        //     'is_active' => true,

        // ]);
        // $table =  DB::table('projects')
        //             // ->select('name')->get();
        //         ->pluck('name');
        $table =  Project::all();
                return view('eloquentViews/project',compact('table'));
        // dd($table);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $factory->define(City::class, function (Faker $faker) {
            return [
                'name' => $faker->city
            ];
        });
        $factory->define(Company::class, function (Faker $faker) {
            return [
                'name' => $faker->company
            ];
        });
        $factory->define(User::class, function (Faker $faker) {
            return [
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail,
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => Str::random(10),
            ];
        });
        $factory->define(Project::class, function (Faker $faker) {
            return [
                'city_id' => rand(1, 80),
                'company_id' => rand(1, 20),
                'user_id' => rand(1, 3),
                'budget' => rand(15000, 90000),
                'name' => $faker->sentence(),
                'execution_date' => $faker->date(),
                'is_active' => $faker->boolean()
            ];
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $project = Project::find(1);
        $project->name = 'Nuevo proyecto de Id 1';
        $project->save();

        return "Actualizado";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete()
    {
        Project::destroy([4]);
        // DB::table('projects')->whereId(5)
        // ->delete();
    }
    public function destroy($id)
    {
    }
}
