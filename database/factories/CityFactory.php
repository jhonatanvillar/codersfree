<?php

namespace Database\Factories;
use App\Models\City;
use Illuminate\Database\Eloquent\Factories\Factory;

class CityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence(), //para una oracion
            // 'name' => $this->faker->paragraph(), // para un parrafo
            // 'name' => $this->faker->randomElement(['Desarrollo','Diseño web']), // para que alterne

        ];
    }
}
