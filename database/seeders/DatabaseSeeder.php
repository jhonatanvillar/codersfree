<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Curso;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Curso::factory(50)->create();
        $this->call(CitiesSeeder::class);
        $this->call(ProjectSeeder::class);
        $this->call(CompaniesSeeder::class);
        $this->call(UsersSeeder::class);

    }
}
