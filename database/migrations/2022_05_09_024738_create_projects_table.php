<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('city_id')->unsigned();
            $table->bigInteger('company_id')->unsigned();
            $table->integer('user_id');
            $table->string('name',30);
            $table->date('execution_date');
            $table->tinyInteger('is_active');
            // $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');  
            // $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}
