@extends('layouts.plantilla')

@section('title', 'editar '.$curso->id)
@section('content')
    <h1> Aqui creas Cursos</h1>
    <form action="{{ route('cursos.update',$curso) }}" method="POST">
        @csrf
        @method('put')
        <div class="mb-3 row">
            <label for="nombre" class="col-sm-2 col-form-label">Nombre</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="nombre" id="nombre" value="{{ $curso->name }}">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="descripcion" class="col-sm-2 col-form-label">Descripcion</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="descripcion" name="descripcion" value="{{ $curso->descripcion }}">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="categoria" class="col-sm-2 col-form-label">Categoria</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="categoria" name="categoria" value="{{ $curso->categoria }}">
            </div>
        </div>
        <div class="mb-3 row">
            <button class="submit" type="submit">Enviar Formulario</button>
        </div>
    </form>
@endsection
