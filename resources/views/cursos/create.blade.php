@extends('layouts.plantilla')

@section('title', 'create')
@section('content')
    <h1> Aqui creas Cursos</h1>
    <form action="{{ route('cursos.store') }}" method="POST">
        @csrf
        <div class="mb-3 row">
            <label for="nombre" class="col-sm-2 col-form-label">Nombre</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="nombre" id="nombre" value="">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="descripcion" class="col-sm-2 col-form-label">Descripcion</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="descripcion" name="descripcion">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="categoria" class="col-sm-2 col-form-label">Categoria</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="categoria" name="categoria">
            </div>
        </div>
        <div class="mb-3 row">
            <button class="submit" type="submit">Enviar Formulario</button>
        </div>
    </form>
@endsection
