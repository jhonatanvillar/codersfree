@extends('layouts.plantilla')

@section('title', 'Cursos')
@section('content')
    <h1 class="bg-dark text-white">Bienvenido a la pagina de Cursos</h1>
    <a href="{{ route('cursos.create') }}">Crear Curso</a>
    <ul>
        @foreach($curso as $value)
        <li>
            <a href="{{ route('cursos.show',$value->id) }}">{{ $value->name }}</a>
            {{-- {{ route('cursos.show',$value->id) }} --}}
        </li>
        @endforeach
    </ul>
    {{ $curso->links() }}
@endsection
